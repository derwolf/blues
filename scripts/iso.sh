#!/bin/sh

#
# Copyright 2017 Daniel Stimpfl
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
# 
#    http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#


mkdir -p isodir/boot/grub
cp sysroot/boot/blues.core isodir/boot/blues.core
cat > isodir/boot/grub/grub.cfg << EOF
menuentry "blues" {
	multiboot /boot/blues.core
}
EOF
grub-mkrescue -o blues.iso isodir
