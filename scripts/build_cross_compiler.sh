#!/bin/zsh

#
# Copyright 2017 Daniel Stimpfl
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
# 
#    http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#


# Load config
#DIR=$(dirname $0)
#if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi
#. "$DIR/config.sh"


verify_sig() {
	local file=$1 

	echo "Verifying $1..."

	local out=$(gpg --status-fd 1 --verify --keyring ./gnu-keyring.gpg binutils-2.28.tar.gz.sig binutils-2.28.tar.gz 2>/dev/null) 
	if [[ "$out" =~ "^\[GNUPG:\] VALIDSIG.*" ]]; then
		return 0
	else 
		echo "$out" >&2
		return 1
	fi
}


download_needed_packages() {
	echo "[*] downloading needed packages..."

	! [[ -a gnu-keyring.gpg ]] && wget ftp://ftp.gnu.org/gnu/gnu-keyring.gpg
	! [[ -a binutils-2.28.tar.gz ]] && wget http://ftp.gnu.org/gnu/binutils/binutils-2.28.tar.gz
	! [[ -a binutils-2.28.tar.gz.sig ]] && wget http://ftp.gnu.org/gnu/binutils/binutils-2.28.tar.gz.sig
	! [[ -a gcc-7.1.0.tar.bz2 ]] && wget http://mirror.netcologne.de/gnu/gcc/gcc-7.1.0/gcc-7.1.0.tar.bz2
	! [[ -a gcc-7.1.0.tar.bz2.sig ]] && wget http://mirror.netcologne.de/gnu/gcc/gcc-7.1.0/gcc-7.1.0.tar.bz2.sig

	# verify_sig binutils-2.28.tar.gz;
	# verify_sig gcc-7.1.0.tar.bz2;

	echo "[*] extracting packages..."
	! [[ -a binutils-2.28 ]] && tar xf binutils-2.28.tar.gz
	! [[ -a gcc-7.1.0 ]] && tar xf gcc-7.1.0.tar.bz2

	pushd gcc-7.1.0
	./contrib/download_prerequisites
	popd
}

build_binutils() {
	echo "[*] building binutils..."

	# binutils
	pushd binutils-2.28
	mkdir -pv build
	pushd build
	../configure --target=$TARGET --prefix=$PREFIX --with-sysroot --disable-nls --disable-werror
	make -j4
	make install
	popd
	rm -rf build
	popd
}


build_gcc() {
	echo "[*] building gcc..."

	pushd gcc-7.1.0
	mkdir -pv build
	pushd build
	../configure --target=$TARGET --prefix=$PREFIX --disable-nls --enable-languages=c,c++ --without-headers
	make all-gcc -j4
	make all-target-libgcc -j4
	make install-gcc
	make install-target-libgcc
	
	popd
	rm -rf build
	popd
}


echo "[*] building cross compiler at `pwd`"


mkdir -pv build_cross
pushd build_cross

download_needed_packages
build_binutils
build_gcc

popd

# rm -rf build_cross

echo "[X] cross compiler successfully compiled and installed!"

