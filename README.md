# blues operating system
## Quickstart
### To build blues:
	make
### To build the cross compiler
	make cross

## Configuring the build
Use following variables to configure your build:

Set ARCH to your target machines architecture (look into core/arch for all supported architectures)

	make ARCH=i386
	# make ARCH=arm
	...

Set PLATFORM to your target machines platform. (look into core/arch/ARCH/ for all supported platforms for a given architecture)

	make ARCH=i386 PLATFORM=pc
	# make ARCH=arm PLATFORM=raspberrypi
	...

Set CROSS_TARGET to the target for the cross compiler

	make CROSS_TARGET=i686 cross
	# make CROSS_TARGET=arm cross
	...
