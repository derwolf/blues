#
# Copyright 2017 Daniel Stimpfl
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
# 
#    http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#


ARCH ?= i386
PLATFORM ?= pc

CROSS_TARGET ?= $(ARCH)
CROSS_PREFIX ?= $(HOME)/opt/cross

PATH := $(CROSS_PREFIX)/bin:$(PATH)

CC := $(CROSS_TARGET)-elf-gcc
CXX := $(CROSS_TARGET)-elf-g++
AS := $(CROSS_TARGET)-elf-as
LD := $(CROSS_TARGET)-elf-ld


PROJECTS := core

PREFIX := $(shell pwd)/sysroot


.PHONY: all clean iso cross

all: $(PROJECTS)
	$(MAKE) -C $< all ARCH=$(ARCH) PLATFORM=$(PLATFORM) CC=$(CC) CXX=$(CXX) AS=$(AS) LD=$(LD)

cleanall: clean

clean: $(PROJECTS)
	$(MAKE) -C $< clean CC=$(CC) CXX=$(CXX) AS=$(AS) LD=$(LD)

distclean: cleanall
	rm -rf sysroot
	rm -rf isodir
	rm -rf build_cross
	rm -f blues.iso

install: $(PROJECTS)
	mkdir -p $(PREFIX)/boot
	$(MAKE) -C $< install PREFIX=$(PREFIX) CC=$(CC) CXX=$(CXX) AS=$(AS) LD=$(LD)

iso: install
	./scripts/iso.sh

run: iso
	qemu-system-"$(ARCH)" -cdrom blues.iso

run-curses: iso
	qemu-system-x86_64 -curses -cdrom blues.iso

debug: iso
	qemu-system-"$(ARCH)" -s -S -cdrom blues.iso
	gdb --command=gdb.cmds

cross:
	TARGET="$(CROSS_TARGET)-elf" PREFIX="$(CROSS_PREFIX)" ./scripts/build_cross_compiler.sh
