/*
 * Copyright 2017 Daniel Stimpfl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

#include <arch/i386/interrupts.h>
#include <arch/i386/cpu_state.h>
#include <arch/i386/idt.h>
#include <arch/i386/pc/pic.h>

#include <core.h>
#include <term.h>

static Idt idt;
static Pic pic;


// Exceptions
extern uint32_t int_stub_0;
extern uint32_t int_stub_1;
extern uint32_t int_stub_2;
extern uint32_t int_stub_3;
extern uint32_t int_stub_4;
extern uint32_t int_stub_5;
extern uint32_t int_stub_6;
extern uint32_t int_stub_7;
extern uint32_t int_stub_8;
extern uint32_t int_stub_9;
extern uint32_t int_stub_10;
extern uint32_t int_stub_11;
extern uint32_t int_stub_12;
extern uint32_t int_stub_13;
extern uint32_t int_stub_14;
extern uint32_t int_stub_15;
extern uint32_t int_stub_16;
extern uint32_t int_stub_17;
extern uint32_t int_stub_18;

// IRQs
extern uint32_t int_stub_32;
extern uint32_t int_stub_33;
extern uint32_t int_stub_34;
extern uint32_t int_stub_35;
extern uint32_t int_stub_36;
extern uint32_t int_stub_37;
extern uint32_t int_stub_38;
extern uint32_t int_stub_39;
extern uint32_t int_stub_40;
extern uint32_t int_stub_41;
extern uint32_t int_stub_42;
extern uint32_t int_stub_43;
extern uint32_t int_stub_44;
extern uint32_t int_stub_45;
extern uint32_t int_stub_46;
extern uint32_t int_stub_47;

// Syscall
extern uint32_t int_stub_80;


static I386CpuState state;

void I386Interrupts::init()
{
	ITerminal& term = core.sys.term();
	
	term << "INT(i386) - installing IDT..." << endl;
	install_idt();

	term << "INT(i386) - remapping PIC..." << endl;
	pic.remap_irqs(IRQ_0, IRQ_8);

	term << "INT(i386) - DONE." << endl;
}

void I386Interrupts::enable()
{
	asm volatile ("sti\n");
}

void I386Interrupts::disable()
{
	asm volatile ("cli\n");
}

void I386Interrupts::handle(uint32_t intno, uint32_t error, I386CpuState& state)
{
	UNUSED(error);

	ITerminal& term = core.sys.term();
	IIO& io = core.sys.io();

	// if it is an IRQ, reset PIC
	if ((intno >= IRQ_0) && (intno <= IRQ_15))
	{
		if (intno >= IRQ_8)
		{
			io.outb(0xA0, 0x20);
		}
		io.outb(0x20, 0x20);
	}

	ICpuState* state_ptr = (ICpuState*)&state;
/*
#define INT_DIVIDE_BY_ZERO 		0
#define INT_DEBUG 			1
#define INT_NMI 			2
#define INT_BREAK_POINT 		3
#define	INT_OVERFLOW 			4
#define INT_BOUND_RANGE_EXCEEDED 	5
#define INT_INVALID_OPCODE 		6
#define INT_DEVICE_NOT_AVAILABLE 	7
#define INT_DOUBLE_FAULT 		8
#define INT_CO_PROC_SEG_OVERRUN 	9
#define INT_INVALID_TSS			10
#define INT_SEG_NOT_PRESENT		11
#define	INT_STACK_SEG_FAULT		12
#define INT_GENERAL_PROTECTION_FAULT	13
#define INT_PAGE_FAULT			14
#define INT_X87_FLOATING_POINT_EXC 	15
#define INT_ALGINMENT_CHECK		16
#define INT_MACHINE_CHECK		17
*/

	switch(intno)
	{
		case IRQ_0:
			// Timer-Interrupt
			term << ".";
			break;
		
		case INT_DIVIDE_BY_ZERO:
			core.panic("Division by zero!", state_ptr);
			break;
		case INT_DOUBLE_FAULT:
			core.panic("Double fault!", state_ptr);
			break;
		case INT_GENERAL_PROTECTION_FAULT:
			core.panic("General Protection Fault!", state_ptr);
			break;
		default:
			core.panic("Unhandled interrupt!", state_ptr);
	}
}


void I386Interrupts::install_idt()
{
	IdtDescriptor desc;
	
	desc.cs_selector = 0x08;
	desc.present = IDT_PRESENT;
	desc.dpl = IDT_DPL_0;
	desc.type = IDT_TYPE_INT_GATE;

	desc.offset = (uint32_t)&int_stub_0;
	idt.set_descriptor(0, desc);
	desc.offset = (uint32_t)&int_stub_1;
	idt.set_descriptor(1, desc);
	desc.offset = (uint32_t)&int_stub_2;
	idt.set_descriptor(2, desc);
	desc.offset = (uint32_t)&int_stub_3;
	idt.set_descriptor(3, desc);
	desc.offset = (uint32_t)&int_stub_4;
	idt.set_descriptor(4, desc);
	desc.offset = (uint32_t)&int_stub_5;
	idt.set_descriptor(5, desc);
	desc.offset = (uint32_t)&int_stub_6;
	idt.set_descriptor(6, desc);
	desc.offset = (uint32_t)&int_stub_7;
	idt.set_descriptor(7, desc);
	desc.offset = (uint32_t)&int_stub_8;
	idt.set_descriptor(8, desc);
	desc.offset = (uint32_t)&int_stub_9;
	idt.set_descriptor(9, desc);
	desc.offset = (uint32_t)&int_stub_10;
	idt.set_descriptor(10, desc);
	desc.offset = (uint32_t)&int_stub_11;
	idt.set_descriptor(11, desc);
	desc.offset = (uint32_t)&int_stub_12;
	idt.set_descriptor(12, desc);
	desc.offset = (uint32_t)&int_stub_13;
	idt.set_descriptor(13, desc);
	desc.offset = (uint32_t)&int_stub_14;
	idt.set_descriptor(14, desc);
	desc.offset = (uint32_t)&int_stub_15;
	idt.set_descriptor(15, desc);
	desc.offset = (uint32_t)&int_stub_16;
	idt.set_descriptor(16, desc);
	desc.offset = (uint32_t)&int_stub_17;
	idt.set_descriptor(17, desc);
	desc.offset = (uint32_t)&int_stub_18;
	idt.set_descriptor(18, desc);

	desc.offset = (uint32_t)&int_stub_32;
	idt.set_descriptor(32, desc);
	desc.offset = (uint32_t)&int_stub_33;
	idt.set_descriptor(33, desc);
	desc.offset = (uint32_t)&int_stub_34;
	idt.set_descriptor(34, desc);
	desc.offset = (uint32_t)&int_stub_35;
	idt.set_descriptor(35, desc);
	desc.offset = (uint32_t)&int_stub_36;
	idt.set_descriptor(36, desc);
	desc.offset = (uint32_t)&int_stub_37;
	idt.set_descriptor(37, desc);
	desc.offset = (uint32_t)&int_stub_38;
	idt.set_descriptor(38, desc);
	desc.offset = (uint32_t)&int_stub_39;
	idt.set_descriptor(39, desc);
	desc.offset = (uint32_t)&int_stub_40;
	idt.set_descriptor(40, desc);
	desc.offset = (uint32_t)&int_stub_41;
	idt.set_descriptor(41, desc);
	desc.offset = (uint32_t)&int_stub_42;
	idt.set_descriptor(42, desc);
	desc.offset = (uint32_t)&int_stub_43;
	idt.set_descriptor(43, desc);
	desc.offset = (uint32_t)&int_stub_44;
	idt.set_descriptor(44, desc);
	desc.offset = (uint32_t)&int_stub_45;
	idt.set_descriptor(45, desc);
	desc.offset = (uint32_t)&int_stub_46;
	idt.set_descriptor(46, desc);
	desc.offset = (uint32_t)&int_stub_47;
	idt.set_descriptor(47, desc);


	idt.install();
}



