/*
 * Copyright 2017 Daniel Stimpfl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

#include <core.h>
#include <term.h>
#include <arch/i386/cpu_state.h>


void I386CpuState::assign(cpu_state_ptr state)
{
	m_state.eax = state->eax;
	m_state.ebx = state->ebx;
	m_state.ecx = state->ecx;
	m_state.edx = state->edx;
	m_state.esi = state->esi;
	m_state.edi = state->edi;
	m_state.ebp = state->ebp;

	m_state.intno = state->intno;
	m_state.error = state->error;

	m_state.eip = state->eip;
	m_state.cs = state->cs;
	m_state.eflags = state->eflags;
	//m_state.esp = state->esp;
	//m_state.ss = state->ss;
	m_state.esp = 0xBAD00000;
	m_state.ss = 0xBAD00000;
}

void I386CpuState::print(ITerminal& term)
{
	term << "Int: " << hex((unsigned int)m_state.intno, 2) << "          Error: " << hex((unsigned int)m_state.error, 8) << endl;

	if (m_state.error > 0)
	{
		if (m_state.error & 1)
			term << "Ext.: Yes";
		else
			term << "Ext.: No";
	
		term << " Tbl.: "; 
		switch((m_state.error >> 1) & 0x3)
		{
			case 0:
				term << "GDT";
				break;
			case 1:
			case 3:
				term << "IDT";
				break;
			case 2:
				term << "LDT";
				break;
			default:
				term << "N/A";
		};

		term << " Index: " << hex((unsigned int)m_state.error >> 3) << endl << endl;
	}

	term << "EAX: " << hex((unsigned int)m_state.eax, 8) << "      EBX: " << hex((unsigned int)m_state.ebx, 8) << endl;
	term << "ECX: " << hex((unsigned int)m_state.ecx, 8) << "      EDX: " << hex((unsigned int)m_state.edx, 8) << endl;
	term << "ESI: " << hex((unsigned int)m_state.esi, 8) << "      EDI: " << hex((unsigned int)m_state.edi, 8) << endl;
	term << "EBP: " << hex((unsigned int)m_state.ebp, 8) << endl;
	term << "EIP: " << hex((unsigned int)m_state.eip, 8) << "       CS: " << hex((unsigned int)m_state.cs, 8) << endl;
	term << "ESP: " << hex((unsigned int)m_state.esp, 8) << "       SS: " << hex((unsigned int)m_state.ss, 8) << endl;
	term << "EFLAGS: " << hex((unsigned int)m_state.eflags, 8) << endl;
	term << endl;

}
