/*
 * Copyright 2017 Daniel Stimpfl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

#include <core.h>
#include <stdint.h>
#include <cpu_state.h>
#include <arch/i386/idt.h>
#include <arch/i386/cpu_state.h>
#include <arch/i386/interrupts.h>


// static memory
static uint8_t idt_table[IDT_MAX_ENTRIES << 3]; 


struct idt_ptr_t
{
	uint16_t limit;
	void *pointer;
} __attribute__((packed)) idtp;

Idt::Idt()
{
	// Set all entries to zero
	for (int i=0; i < IDT_MAX_ENTRIES << 3; i++)
	{
		idt_table[i] = 0x00;
	}
}

void Idt::set_descriptor(int index, IdtDescriptor desc)
{
	if ((index < 0) || (index >= IDT_MAX_ENTRIES))
	{
		core.panic("Invalid INT-Index", 0);
	}

	uint8_t* target = (uint8_t*)(idt_table + (index << 3));
	target[0] = desc.offset & 0xFF;
	target[1] = (desc.offset >> 8) & 0xFF;
	target[2] = desc.cs_selector & 0xFF;
	target[3] = (desc.cs_selector >> 8) & 0xFF;
	
	target[4] = 0x00; // no Longmode :)
	target[5] = ((desc.present & 0x1) << 7) | ((desc.dpl & 0x3) << 5) | 0x0 | 0x8 | (desc.type & 0x7);
	target[6] = (desc.offset >> 16) & 0xFF;
	target[7] = (desc.offset >> 24) & 0xFF;
	
}

void Idt::install()
{
	idtp.limit = (IDT_MAX_ENTRIES << 3) - 1;
	idtp.pointer = idt_table;
	asm volatile ("lidt %0\n"::"m"(idtp));
}
