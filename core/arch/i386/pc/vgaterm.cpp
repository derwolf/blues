/*
 * Copyright 2017 Daniel Stimpfl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */


#include <core.h>
#include <term.h>
#include <arch/i386/pc/vgaterm.h>
#include <io.h>


void VGATerminal::set_cursor(int x, int y)
{
	unsigned short pos=(y*TERM_MAX_X) + x;
	
	IIO& io = core.sys.io();

	io.outb(0x3D4, 0x0F);
	io.outb(0x3D5, (unsigned char)(pos & 0xFF));
	io.outb(0x3D4, 0x0E);
	io.outb(0x3D5, (unsigned char)((pos >> 8) & 0xFF));
}

void VGATerminal::putch(char c)
{

	switch(c)
	{
		case '\n':
			m_x = TERM_MAX_X;
			break;
		case '\b':
			m_x--;
			break;
		default:
			screen[(m_y*TERM_MAX_X+m_x)*2] = c;
			screen[(m_y*TERM_MAX_X+m_x)*2+1] = m_attr;
			m_x++;
	}

	if (m_x >= TERM_MAX_X)
	{
		m_x = 0;
		m_y++;
	}
	if (m_x < 0)
	{
		m_x = TERM_MAX_X -1;
		m_y--;
	}

	if (m_y >= TERM_MAX_Y)
		m_y = 0;
	if (m_y < 0)
		m_y = 0;

	set_cursor(m_x, m_y);
}


void VGATerminal::clr()
{
	m_x = 0;
	m_y = 0;
	for (int y = 0; y < TERM_MAX_Y; y++)
		for (int x = 0; x < TERM_MAX_X; x++)
		{
			putch(' ');
		}
	set_cursor(m_x, m_y);
}

