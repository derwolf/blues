/*
 * Copyright 2017 Daniel Stimpfl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */


#include <core.h>
#include <system.h>
#include <cpu.h>
#include <term.h>
#include <io.h>
#include <arch/i386/pc/vgaterm.h>
#include <arch/i386/cpu.h>
#include <arch/i386/io.h>

static VGATerminal vgaterm;
static I386Cpu i386cpu;
static I386Io i386io;

void System::init()
{
	vgaterm << "Initializing System..." << endl;
	i386cpu.init();
	vgaterm << "System initialized." << endl;
}

ICPU& System::cpu()
{
	return i386cpu;
}

ITerminal& System::term()
{
	return vgaterm;
}

IIO& System::io()
{
	return i386io;
}

