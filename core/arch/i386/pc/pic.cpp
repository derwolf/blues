/*
 * Copyright 2017 Daniel Stimpfl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

#include <core.h>
#include <arch/i386/pc/pic.h>
#include <io.h>

void Pic::remap_irqs(int start_master, int start_slave)
{
	IIO& io = core.sys.io();

	io.outb(0x20, 0x11);
	io.outb(0x21, start_master & 0xFF);
	io.outb(0x21, 0x04);
	io.outb(0x21, 0x01);

	io.outb(0xA0, 0x11);
	io.outb(0xA1, start_slave);
	io.outb(0xA1, 0x02);
	io.outb(0xA1, 0x01);

	io.outb(0x21, 0x00);
	io.outb(0xA1, 0x00);
}
