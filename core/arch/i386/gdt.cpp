/*
 * Copyright 2017 Daniel Stimpfl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

#include <stdint.h>
#include <core.h>
#include <arch/i386/gdt.h>

#define GDT_ENTRIES_MAX 5

// Static Memory
static uint8_t gdt_table[GDT_ENTRIES_MAX << 3];

struct gdt_ptr_t {
	uint16_t limit;
	void* pointer;
} __attribute__((packed)) gdt_ptr;


void Gdt::set_descriptor(int index, GdtDescriptor desc)
{
	if ((index < 0) || (index >= GDT_ENTRIES_MAX))
	{
		core.panic("GDT-Index out of range.", 0);
	}
	
	uint8_t* target = (uint8_t*)(gdt_table + (index << 3));
	
	// Granularity
	if (desc.limit > 0xFFFF)
	{
		desc.limit >>= 12;
		target[6] = 0xC0;
	} else {
		target[6] = 0x40;
	}
	
	// Limit
	target[0] = desc.limit & 0xFF;
	target[1] = (desc.limit >> 8) & 0xFF;
	target[6] |= (desc.limit >> 16) & 0x0F;
	
	// Base
	target[2] = desc.base & 0xFF;
	target[3] = (desc.base >> 8) & 0xFF;
	target[4] = (desc.base >> 16) & 0xFF;
	target[7] = (desc.base >> 24) & 0xFF;
	
	// Type
	target[5] = desc.type & 0xFF;
}

void Gdt::install()
{
	gdt_ptr.limit = GDT_ENTRIES_MAX * 8 - 1;
	gdt_ptr.pointer = (void*)gdt_table;

	asm volatile("lgdt %0\n"::"m"(gdt_ptr));
	
	asm volatile(
		"mov $0x10, %ax\n"
		"mov %ax, %ds\n"
		"mov %ax, %es\n"
		"mov %ax, %fs\n"
		"mov %ax, %gs\n"
		"mov %ax, %ss\n"
		"ljmp $0x8, $l\n"
		"l:\n"
	);
}
