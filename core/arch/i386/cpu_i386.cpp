/*
 * Copyright 2017 Daniel Stimpfl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

#include <stdint.h>
#include <cpu.h>
#include <core.h>
#include <arch/i386/cpu.h>
#include <arch/i386/gdt.h>
#include <arch/i386/interrupts.h>

static Gdt gdt;
static I386Interrupts ints;
static I386CpuState state;

void I386Cpu::init()
{
	// Setup Interrupttables, Descriptortables, Taskstatesegments, ...
	ITerminal& term = core.sys.term();

	term << "CPU(i386) - installing GDT..." << endl;
	install_gdt();

	term << "CPU(i386) - setting up interrupts..." << endl;
	ints.init();

	term << "CPU(i386) - done." << endl;
}

void I386Cpu::install_gdt()
{
	GdtDescriptor nul_desc;
	nul_desc.base = 0x00000000;
	nul_desc.limit = 0x00000000;
	nul_desc.type = 0x00;

	GdtDescriptor core_code_desc;
	core_code_desc.base = 0x00000000;
	core_code_desc.limit = 0xFFFFFFFF;
	core_code_desc.type = 0x9A;

	GdtDescriptor core_data_desc;
	core_data_desc.base = 0x00000000;
	core_data_desc.limit = 0xFFFFFFFF;
	core_data_desc.type = 0x92;

	GdtDescriptor tss_desc;
	tss_desc.base = 0x00000000;
	tss_desc.limit = 0x00000000;
	tss_desc.type = 0x00;

	gdt.set_descriptor(0, nul_desc);
	gdt.set_descriptor(1, core_code_desc);
	gdt.set_descriptor(2, core_data_desc);
	gdt.set_descriptor(3, tss_desc);

	gdt.install();
}

IInterrupts& I386Cpu::interrupts()
{
	return ints;
}



extern "C"
void handle_interrupt(cpu_state_ptr cpu_state)
{
	state.assign(cpu_state);
	ints.handle(cpu_state->intno, cpu_state->error, state);
}
