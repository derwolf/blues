/*
 * Copyright 2017 Daniel Stimpfl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

#ifndef _TERM_H
#define _TERM_H 1

#define endl '\n'

enum term_colors_t {
	BLACK = 0x00,
	BLUE = 0x01,
	GREEN = 0x02,
	CYAN = 0x03,
	RED = 0x04,
	MAGENTA = 0x05,
	BROWN = 0x06,
	GRAY = 0x07,
	DARK_GRAY = 0x08,
	BRIGHT_BLUE = 0x09,
	BRIGHT_GREEN = 0x0A,
	BRIGHT_CYAN = 0x0B,
	BRIGHT_RED = 0x0C,
	BRIGHT_MAGENTA = 0x0D,
	YELLOW = 0x0E,
	WHITE = 0x0F
};

typedef
struct hex_s {
	int value;
	bool sign;
	int digits;
	hex_s(int v) {
		value = v;
		sign = true;
		digits = 0;
	}
	hex_s(unsigned int v) {
		value = (signed)v;
		sign = false;
		digits = 0;
	}
	hex_s(int v, int d) {
		value = v;
		sign = true;
		digits = d;
	}
	hex_s(unsigned int v, int d) {
		value = v;
		sign = false;
		digits = d;
	}
} hex;

typedef
struct ch_attr {
	term_colors_t foreground;
	term_colors_t background;
	ch_attr(term_colors_t f, term_colors_t b) {
		foreground = f;
		background = b;
	}
} cattr;


class ITerminal
{
	public:
		virtual ITerminal& operator<<(const char *) = 0;
		virtual ITerminal& operator<<(hex) = 0;
		virtual ITerminal& operator<<(const char) = 0;
		virtual ITerminal& operator<<(cattr) = 0;
		virtual ITerminal& operator<<(int) = 0;
		virtual void clear() = 0;
};


class Terminal : public ITerminal
{
	protected:
		char m_attr;
		virtual void putch(char) {};
		virtual void clr() {};
	public:
		ITerminal& operator<<(const char *);
		ITerminal& operator<<(hex);
		ITerminal& operator<<(const char);
		ITerminal& operator<<(cattr);
		ITerminal& operator<<(int);
		void clear();
};



#endif
