/*
 * Copyright 2017 Daniel Stimpfl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

#ifndef _CPU_I386_H
#define _CPU_I386_H

#include <cpu.h>
#include <interrupts.h>
#include <arch/i386/gdt.h>

#include <stdint.h>

class I386Cpu : public ICPU
{
private:
	void install_gdt();
public:
	void init();
	IInterrupts& interrupts();

	
};

#endif
