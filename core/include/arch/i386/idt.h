/*
 * Copyright 2017 Daniel Stimpfl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

#ifndef _IDT_H
#define _IDT_H

#include <stdint.h>

#include <arch/i386/interrupts.h>

#define IDT_MAX_ENTRIES 256


#define IDT_TYPE_INT_GATE 0x06
#define IDT_TYPE_TRAP_GATE 0x07
#define IDT_TYPE_TASK_GATE 0x05

#define IDT_PRESENT 0x01
#define IDT_NOT_PRESENT 0x00

#define IDT_DPL_0 0x00
#define IDT_DPL_1 0x01
#define IDT_DPL_2 0x02

class IdtDescriptor
{
public:
	uint32_t offset;
	int cs_selector;
	int type;
	int dpl;
	int present;
};

class Idt
{
public:
	Idt();

	void set_descriptor(int index, IdtDescriptor desc);
	void install();
};

#endif
