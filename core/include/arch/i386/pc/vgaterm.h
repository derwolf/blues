/*
 * Copyright 2017 Daniel Stimpfl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

#ifndef _VGA_TERM_H
#define _VGA_TERM_H 1

#include <term.h>

#define TERM_MAX_X 80
#define TERM_MAX_Y 25

class VGATerminal: public Terminal
{
	private:
		int m_x;
		int m_y;
		unsigned char* screen;
		void set_cursor(int x, int y);
	protected:
		void putch(char);
		void clr();
	public:
		VGATerminal(): m_x(0), m_y(0), screen((unsigned char*)0xB8000) { m_attr = 0x0F; };
};

#endif
