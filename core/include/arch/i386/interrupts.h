/*
 * Copyright 2017 Daniel Stimpfl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

#ifndef __INTERRUPT_I386_H
#define __INTERRUPT_I386_H

#include <interrupts.h>

#include <arch/i386/cpu_state.h>

// x86 Exceptions
#define INT_DIVIDE_BY_ZERO 		0
#define INT_DEBUG 			1
#define INT_NMI 			2
#define INT_BREAK_POINT 		3
#define	INT_OVERFLOW 			4
#define INT_BOUND_RANGE_EXCEEDED 	5
#define INT_INVALID_OPCODE 		6
#define INT_DEVICE_NOT_AVAILABLE 	7
#define INT_DOUBLE_FAULT 		8
#define INT_CO_PROC_SEG_OVERRUN 	9
#define INT_INVALID_TSS			10
#define INT_SEG_NOT_PRESENT		11
#define	INT_STACK_SEG_FAULT		12
#define INT_GENERAL_PROTECTION_FAULT	13
#define INT_PAGE_FAULT			14
#define INT_X87_FLOATING_POINT_EXC 	15
#define INT_ALGINMENT_CHECK		16
#define INT_MACHINE_CHECK		17
#define INT_SIMD_FLOATING_POINT_EXC	18

#define IRQ_0	32
#define IRQ_1	33
#define IRQ_2	34
#define IRQ_3	35
#define IRQ_4	36
#define IRQ_5	37
#define IRQ_6	38
#define IRQ_7	39
#define IRQ_8	40
#define IRQ_10	41
#define IRQ_11	42
#define IRQ_12	43
#define IRQ_13	44
#define IRQ_14	45
#define IRQ_15	46


class I386Interrupts : public IInterrupts
{
private:
	void install_idt();
public:
	void init();

	void handle(uint32_t, uint32_t, I386CpuState&);
	void enable();
	void disable();
};

#endif
