/*
 * Copyright 2017 Daniel Stimpfl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

#ifndef _CORE_H
#define _CORE_H 1

#define DEBUG_BPOINT __asm__("hlt\n")

#define UNUSED(x) (void)x

#include <system.h>
#include <term.h>
#include <cpu_state.h>

class Core
{
	public:
		System sys;
		void print_welcome();
		void run();
		void panic(const char*, ICpuState*);
};

extern Core core;

#endif
