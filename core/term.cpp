/*
 * Copyright 2017 Daniel Stimpfl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */


#include <term.h>

ITerminal& Terminal::operator<< (const char *string)
{
	while (*string)
	{
		putch(*string++);
	}
	return *this;
}


ITerminal& Terminal::operator<< (hex v)
{
	static char h[16] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
	char tmp[sizeof(int) << 1];
	
	int c = 0;
	int div = 16;

	if (v.sign) {
		int val = v.value;

		if (val < 0)
			*this << "-0x";
		else
			*this << "0x";

		if (val == 0) {
			*this << '0';
			while ((c < v.digits -1) && ((unsigned)c  < (sizeof(int) << 1))) {
				*this << '0';
				c++;
			}
			return *this;
		}

		while (val) {
			if (val < 0)
				tmp[c] = h[-(val % div)];
			else
				tmp[c] = h[(val % div)];
			c++;
			val /= div;
		}
		while ((c < v.digits) && ((unsigned)c  < (sizeof(int) << 1)))
			tmp[c++] = '0';

	} else {
		*this << "0x";
		unsigned int val = v.value;
		if (val == 0) {
			*this << '0';
			while ((c < v.digits -1) && ((unsigned)c  < (sizeof(int) << 1))) {
				*this << '0';
				c++;
			}
			return *this;
		}
		while (val) {
			tmp[c] = h[(val % div)];
			c++;
			val /= div;
		}
		while ((c < v.digits) && ((unsigned)c  < (sizeof(int) << 1)))
			tmp[c++] = '0';
	}

	for (int i = c-1; i >= 0; i--)
		putch(tmp[i]);
	return *this;
}

ITerminal& Terminal::operator<< (const char c)
{
	putch(c);
	return *this;
}

ITerminal& Terminal::operator<<(cattr a)
{
	m_attr = (a.background << 4) | a.foreground;
	return *this;
}

ITerminal& Terminal::operator<<(int v)
{
	char tmp[sizeof(int)*2];
	int c = 0;

	if (v == 0)
	{
		putch('0');
		return *this;
	}
	
	if (v < 0)
		putch('-');

	while (v)
	{
		if (v < 0)
			tmp[c] = '0' - (v % 10);
		else
			tmp[c] = '0' + (v % 10);
		c++;
		v /= 10;
	}	

	for (int i=c-1; i >= 0; i--)
		putch(tmp[i]);

	return *this;
}

void Terminal::clear()
{
	clr();
}

