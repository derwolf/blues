/*
 * Copyright 2017 Daniel Stimpfl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

#include <core.h>

typedef void (*ctor)();

extern "C" ctor start_ctors;
extern "C" ctor end_ctors;


extern "C"
void __cxa_pure_virtual()
{
	// TODO: abort
	for (;;);
}

extern "C"
void init_ctors()
{
	for (ctor* c = &start_ctors; c != &end_ctors; ++c)
		(*c)();
}


Core core;


extern "C"
void core_main()
{
	core.run();
	
	// This code should never be reached
	for (;;);
}
