/*
 * Copyright 2017 Daniel Stimpfl
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

#include <core.h>
#include <cpu_state.h>

void Core::print_welcome()
{
	ITerminal& term = sys.term();
	term << cattr(BLACK, BLACK);
	term.clear();
	term << cattr(GRAY, BLUE) << "blu" << cattr(BLUE, BLACK) << "es";
	term << cattr(DARK_GRAY, BLACK) << "   v" << MAJOR << "." << MINOR << "." << BUGFIX << endl << endl;
}

void Core::panic(const char* error_msg, ICpuState* state)
{
	ITerminal& term = sys.term();

	term << cattr(WHITE, RED) << endl << endl;
	term << "PANIC: " << error_msg << endl;

	if (state != 0)
		state->print(term);

	asm volatile ("cli\n");
	for (;;)
		asm volatile ("hlt");
}


void Core::run()
{
	print_welcome();

	sys.init();

	ITerminal& term = sys.term();
	term << cattr(BLACK, GREEN);
	// term << endl << "Testing interrupts, you should see a lots of '.'s" << endl << "and after short timee a division by zero exception...";
    term << endl << "Let's test. First a lot of '.'s." << endl << "Then a division by zero exception.";
	term << cattr(DARK_GRAY, BLACK) << endl;

	// Last but not least, enable interrupts...
	sys.cpu().interrupts().enable();

	// Test Div By Zero
	int x = 0;
	int y = 0;

	// Wait a bit...
	for (int i = 0; i < 1000000000; i++);

	x /= y;
}
